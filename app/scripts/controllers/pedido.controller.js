define(['run', 'factory/pedido.factory', 'factory/produto.factory'], function() {

    'use strict';

    return ['$scope', '$rootScope', '$stateParams', '$state', '$filter', '$interval', 'BaseController', 'Pedido','Produto',
        function($scope, $rootScope, $stateParams, $state, $filter, $interval, BaseController, Pedido, Produto) {

          console.log('Carregou controller');

            angular.extend($scope, BaseController);

            console.log('Pedido Controller Loaded');
            $scope.pedido = new Pedido();
            $scope.newpedido = new Pedido();
            $scope.produto = new Produto();

            $scope.page = 0;
            $scope.size = 10;
            $scope.sortOrder = 'desc';
            $scope.sortType = 'idPedido';
            $scope.sort = 'idPedido,asc';
            $scope.searchAll = '';

            //********************* CONTROLE DE POOLING *******************//
            $scope.pedidosClienteCart = [];
            $scope.pedidosClienteCartStatic = [];
            $scope.cartCountRefresher;
            $scope.cartIsOn = true;


            $scope.getList = function(value) {
                if ($scope.searchAll !== null && $scope.searchAll !== '' && $scope.searchAll !== undefined) {
                    if (value === 'recalculatePageCounter') {
                        $scope.pedido.listAll($scope.page, $scope.size, $scope.sort, $scope.searchAll, recalculatePageCounter);
                    } else if (value === 'recalculatePages') {
                        $scope.pedido.listAll($scope.page, $scope.size, $scope.sort, $scope.searchAll, recalculatePages);
                    }
                } else {
                    if (value === 'recalculatePageCounter') {
                        $scope.pedido.list($scope.page, $scope.size, $scope.sort, recalculatePageCounter);
                    } else if (value === 'recalculatePages') {
                        $scope.pedido.list($scope.page, $scope.size, $scope.sort, recalculatePages);
                    }
                }
            };

            $scope.$on('$stateChangeSuccess', function(event, toState) {
                console.log('state Change Success');

                if (angular.isDefined($scope.cartCountRefresher)){
                  $interval.cancel($scope.cartCountRefresher);
                }

                $scope.cartCountRefresher =  $interval( function(){
                  console.log('Executando get pedidos');
                  $scope.getPedidosClienteCart();
                }, 1000 );

                if (toState.name === 'pedido') {
                    $scope.getList('recalculatePages');
                } else if (toState.name === 'pedido_edit') {
                    $scope.pedido.load($stateParams.id);
                }
            });

            $scope.$on('$viewContentLoaded', function() {
                console.log('View Content Loaded');
            });

            $scope.editPedido = function(id) {
              console.log(id);
                $scope.editingId = id;
                $scope.pedido.load(id, function(data) {
                    $scope.newpedido = data;
                    $scope.showAdd('update');
                });
            };

            $scope.listAll = function(value) {

                $scope.searchAll = value;
                $scope.getList('recalculatePages');

            };

            $scope.delete = function(id) {
                $scope.pedido.delete(id, function(data, status) {
                    if (status === 200) {
                        showStatus('Empresa excluída com sucesso', 'success', 5000);
                        $scope.getList('recalculatePages');
                    } else {
                        showStatus('Erro: ' + data.message, 'error', 5000);
                    }
                });
            };

            $scope.save = function() {
              var produtos = $filter("filter")($scope.produto.content,{selecionado:true});
              console.log(produtos);
              var object = {'idCliente': 1, 'produtos': produtos};

                $scope.pedido.save(object, function(data, status) {
                    if (status === 200) {
                        $scope.hideAdd();
                        showStatus('Pedidoe adicionado com sucesso', 'success', 5000);
                        $scope.getList('recalculatePages');

                    } else {
                        showStatus('Erro: ' + data.message, 'error', 5000);
                    }
                });

            };

            $scope.update = function() {
              console.log($scope.newpedido);
                $scope.pedido.update($scope.newpedido, function(data, status) {
                    if (status === 200) {
                        $scope.hideAdd();
                        showStatus('Empresa atualizada com sucesso', 'success', 5000);
                        $scope.getList('recalculatePages');
                    } else {
                        showStatus('Erro: ' + data.message, 'error', 5000);
                    }
                });
            };

            $scope.savePedido = function() {
              console.log('Chamou save:'+ $scope.editingId);
                if ($scope.editingId) {
                    $scope.update();
                } else {
                    $scope.save();
                }
            };

            $scope.changePage = function(page) {
                if (page === 'prev') {
                    $scope.page = $scope.page - 1;
                }
                if (page === 'next') {
                    $scope.page = $scope.page + 1;
                }
                if (typeof page === 'number') {
                    $scope.page = page - 1;
                }

                if ($scope.page < 0) {
                    $scope.page = 0;
                }
                if ($scope.page > $scope.pageslength.length - 1) {
                    $scope.page = $scope.pageslength.length - 1;
                }

                console.log(page, $scope.page);

                $scope.getList('recalculatePageCounter');
            };


            $scope.changeSize = function(size) {
                console.log(size);
                $scope.size = size.size;
                $scope.sizesSelect = size;

                $scope.getList('recalculatePages');
            };

            $scope.changeSort = function(type) {
                console.log(type);
                if ($scope.sortOrder === 'desc') {
                    $scope.sortOrder = 'asc';
                } else {
                    $scope.sortOrder = 'desc';
                }

                $scope.sortType = type;

                $scope.sort = $scope.sortType + ',' + $scope.sortOrder;

                $scope.getList('recalculatePages');

            };

            $scope.sizes = [{
                name: '10 linhas',
                size: 10
            }, {
                name: '25 linhas',
                size: 25
            }, {
                name: '50 linhas',
                size: 50
            }, {
                name: '100 linhas',
                size: 100
            }];

            $scope.sizesSelect = $scope.sizes[0];

            $scope.showAdd = function() {
                hideStatus(0);
                $scope.getProdutos();
                if($scope.newpedido.vendivel === null || $scope.newpedido.vendivel === undefined){
                    $scope.newpedido.vendivel;
                }
                $scope.templateURL = 'views/pedido/pedido_add.html';
                $scope.onLoadAdd();
            };


            $scope.showList = function() {
                hideStatus(0);
                $scope.getPedidosClienteCartStatic();
                $scope.templateURL = 'views/pedido/pedido_list.html';
                $scope.onLoadAdd();
            };

            $scope.onLoadAdd = function() {
                setTimeout(function() {
                    angular.element('.crud_add').removeClass('__hidden');
                    if ($scope.editingId) {
                        angular.element('.btn-delete').show();
                    } else {
                        angular.element('.btn-delete').hide();
                    }
                    $scope.$apply();
                }, 100);
            };

            $scope.hideAdd = function() {
                angular.element('.crud_add').addClass('__hidden');
                setTimeout(function() {
                    $scope.templateURL = '';
                    $scope.editingId = undefined;
                    $scope.newpedido = {};
                }, 200);
            };

            $scope.editPedido = function(id) {
                $scope.editingId = id;
                $scope.pedido.load(id, function(data) {
                    $scope.newpedido = data;
                    $scope.showAdd('update');
                });
            };

            $scope.showexclusionmodal = function() {
                $scope.modalURL = 'views/crud/exclusion_modal.html';
                angular.element('.app-modal').removeClass('__hidden');
            };

            $scope.confirmExclusion = function() {
                $scope.modalURL = '';
                $scope.delete($scope.editingId);
                angular.element('.app-modal').addClass('__hidden');
                $scope.hideAdd();
            };

            $scope.cancelExclusion = function() {
                $scope.modalURL = '';
                angular.element('.app-modal').addClass('__hidden');
            };

            $scope.hasLogo = function(pedido) {
                if (pedido.logo === null || !pedido.logo) {
                    pedido.logo = 'images/company-placeholder.png';
                }
                return true;
            };

            $scope.getProdutos = function(){
              $scope.produto.vendivel($scope.page, $scope.size, 'descricao,asc', recalculatePages);
            };

            $scope.getPedidosCliente = function () {

                   $scope.pedidosCliente = [];

                       $scope.pedido.pedidosCliente($scope.page, $scope.size, 'idPedido,asc', 1, function (data, status) {
                         if (status === 200 || status === 201){
                           $scope.pedidosCliente = data;
                         }else{
                           console.log('Erro ao carregar pedidos do cliente!');
                         }
                       });


           };

           $scope.getPedidosClienteCart = function () {

                  $scope.pedido.pedidosClienteCart($scope.page, $scope.size, 'idPedido,asc', 1, function (data, status) {
                        if (status === 200 || status === 201){
                          $scope.pedidosClienteCart = data;
                          $scope.cartIsOn = true;
                        }else{
                          $scope.cartIsOn = false;
                          console.log('Erro ao carregar pedidos do cliente!');
                        }
                      });


          };

          $scope.getPedidosClienteCartStatic = function () {

                 $scope.pedido.pedidosClienteCart($scope.page, $scope.size, 'idPedido,asc', 1, function (data, status) {
                       if (status === 200 || status === 201){
                         $scope.pedidosClienteCartStatic = data;
                       }else{
                         console.log('Erro ao carregar pedidos do cliente!');
                       }
                     });


         };

         $scope.calcTotal = function(filtered){
              var sum = 0;
              for(var i = 0 ; i<filtered.length ; i++){
                 sum = sum + filtered[i].precoVenda;
              }
              return sum;
          };

          $scope.$on("$destroy", function() {

            if (angular.isDefined($scope.cartCountRefresher)){
              $interval.cancel($scope.cartCountRefresher);
            }
            console.log("Destruiu e liberou recursos!");
          });

            function showStatus(msg, type, time) {
                $scope.statusdiv = angular.element('.status_msg');
                $scope.status_message = msg;
                $scope.statusdiv.removeClass('__hidden');
                if (type === 'error') {
                    $scope.status_msg_class = '__error';
                } else {
                    $scope.status_msg_class = '__success';
                }
                hideStatus(time);
            }

            function hideStatus(time) {
                setTimeout(function() {
                    angular.element('.status_msg').addClass('__hidden');
                    $scope.status_msg_class = '';
                    $scope.$apply();
                }, time);
            }

            function recalculatePages() {
                var pagesize = $scope.pedido.totalPages;
                $scope.pageslength = new Array(pagesize);
                for (var c = 0; c < $scope.pageslength.length; c += 1) {
                    $scope.pageslength[c] = c + 1;
                }
                recalculatePageCounter();
            }

            function recalculatePageCounter() {
                var totalofpage = $scope.size * ($scope.page + 1);
                var paginationmin = 1 + ($scope.size * $scope.page);
                if (totalofpage > $scope.pedido.totalElements) {
                    totalofpage = $scope.pedido.totalElements;
                }
                $scope.totalElements = $scope.pedido.totalElements;
                $scope.pagesCounter = paginationmin + ' - ' + totalofpage;
                if ($scope.page + 1 > $scope.pedido.totalPages) {
                    $scope.changePage($scope.page);
                    return;
                }
            }

        }
    ];
});
