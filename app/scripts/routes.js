define(['angularAMD', 'app'], function (angularAMD, app) {

  'use strict';

  return app.config(['$stateProvider', '$provide', '$urlRouterProvider',

    function ($stateProvider, $provide, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/login');


      $stateProvider
        .state('home', angularAMD.route(
          {
            url: '/home',
            templateUrl: 'views/cliente/cliente.html',
            controllerUrl: 'controllers/cliente.controller'
          }))
        .state('login', angularAMD.route(
          {
            url: '/login',
            templateUrl: 'views/auth/login.html',
            controllerUrl: 'controllers/auth.controller'
          }))
        .state('usuario', angularAMD.route(
              {
                url: '/usuario',
                templateUrl: 'views/usuario/usuario.html',
                controllerUrl: 'controllers/usuario.controller'
          }))
        .state('cliente', angularAMD.route(
            {
              url: '/cliente',
              templateUrl: 'views/cliente/cliente.html',
              controllerUrl: 'controllers/cliente.controller'
            }))
        .state('pedido', angularAMD.route(
                {
                  url: '/pedido',
                  templateUrl: 'views/pedido/pedido.html',
                  controllerUrl: 'controllers/pedido.controller'
        }))
        .state('produto', angularAMD.route(
          {
            url: '/produto',
            templateUrl: 'views/produto/produto.html',
            controllerUrl: 'controllers/produto.controller'
          }));
    }]);

});
